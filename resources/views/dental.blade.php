@extends('partials.masterlayout')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dental</div>

                <div class="panel-body">
                  <table class="table">

                    <thead>
                      <tr>
                        <th></th>
                        <th scope="col">Service</th>
                        <th scope="col">Insurance Pays</th>
                        <th scope="col">You Pay</th>
                        <th scope="col">Total Cost</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Cleaning</td>
                        <td>$47</td>
                        <td>$30</td>
                        <td>$77</td>
                      </tr>

                      <tr>
                        <th scope="row">2</th>
                        <td>Cavity Filling</td>
                        <td>$65</td>
                        <td>$40</td>
                        <td>$105</td>
                      </tr>

                      <tr>
                        <th scope="row">3</th>
                        <td>Root Canal</td>
                        <td>$225</td>
                        <td>$45</td>
                        <td>$270</td>
                      </tr>

                    </tbody>
                  </table>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <a class="btn btn-primary btn-lg btn-block" href="#" role="button">Find a Dentist!</a>
      </div>

    </div>
</div>
@endsection
