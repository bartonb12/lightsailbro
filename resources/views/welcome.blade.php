@extends('partials.masterlayout')

@section('content')
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <h1>Welcome</h1>
         <p>We know that insurance is a fact of life, but it can be hard to understand, and that can dissuade us from taking advantage of the benefits that you pay for. We want to help break down that barrier by translating your benefits into an easy to understand menu where you can know the cost of each doctor visit and procedure up front. You pay for it, now use it and be healthy. </p>
         <a href="/register" class="btn btn-block btn-primary btn-round-lg btn-lg">Get Started!</a>
       </div><!--column-->
     </div><!--row-->
<br>

    </div><!--container-->
@endsection
